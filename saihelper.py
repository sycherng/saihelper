import os

sai_path = os.path.dirname(os.path.realpath(__file__)) #current directory
blotmap_list = os.listdir(sai_path + '\\blotmap') #list
brushtex_list = os.listdir(sai_path + '\\brushtex') #list
elemap_list = os.listdir(sai_path + '\\elemap') #list
papertex_list = os.listdir(sai_path + '\\papertex') #list

brushform_conf = open('brushform.conf', 'w')
for i in blotmap_list:
    brushform_conf.write('1,blotmap/' + i + '\n')
for i in elemap_list:
    brushform_conf.write('2,elemap/' + i + '\n')
brushform_conf.close()

brushtex_conf = open('brushtex.conf', 'w')
for i in brushtex_list:
    brushtex_conf.write('1,brushtex/' + i + '\n')
brushtex_conf.close()

papertex_conf = open('papertex.conf', 'w')
for i in papertex_list:
    papertex_conf.write('1,papertex/' + i + '\n')
papertex_conf.close()
